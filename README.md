# Subscriber service

## Stack

* Java 21
* Spring boot 3.2
	* Spring Boot Data Jpa
	* Spring Boot Docker Compose
	* Testcontainers
	* Spring Boot observability
* OpenAPI 3 (with Swagger-ui)
* Postgresql
* Flyway
* Observabilty: Opentelemetry
	* Metrics: Prometheus
	* Logging aggregation: Loki
	* Tracing: Zipkin. Jaeger
	* Dashboards: Grafana
* Kubernetes

## How to run the application locally with docker

All commands are run at the root folder of the application.

### Prerequisite

* [Docker](https://docs.docker.com/engine/install/)

### Start the application

```bash
mvn spring-boot:run -Dspring-boot.run.profiles=local
```

This will start the application with *local* profile, [*docker-compose*](https://docs.spring.io/spring-boot/docs/3.2.1/reference/htmlsingle/#features.docker-compose) module will take care of starting all the necessary containers that are declared in *docker-compose.yml* and setting the required properties like: *spring.datasource.url*, when the application is stopped all containers are automatically stopped.

```
-Dspring-boot.run.jvmArguments="-Dprop1=value1 -Dprop2=value2"
```
If you need to set properties as JVM arguments add this line to the previous one.

```
docker compose up
mvn spring-boot:run
```
Alternatively, containers can be started prior to the application, in this case *docker-compose* module will not start them again and will not stop them after the application is stopped.

```bash
mvn spring-boot:test-run
```
This will launch the application using *test* classpath rather than *main*, it will use *testconatainers* instead of *docker-compose* to start containers that are defined in the test configuration class *org.djserr.subscribers.TestcontainerConfig*, more details about testcontainers can be found in the official [documentation](https://docs.spring.io/spring-boot/docs/3.2.1/reference/htmlsingle/#features.testcontainers)

## How to run the application locally with minikube

### Prerequisite

* [Docker](https://docs.docker.com/engine/install/)
* A Docker hub [account](https://hub.docker.com/)
* A kubernetes cluster: [minikube](https://minikube.sigs.k8s.io/docs/start/) can be used but other solutions exist
* [*kubectl*](https://kubernetes.io/docs/tasks/tools/) command line tool installed and configured to use the kubernetes cluster

### Build the OCI image

``` bash
./mvnw spring-boot:build-image
```
Use *./mvnw* on linux/mac, *mvnw.bat* on windows

### Push the image to Docker hub

``` bash
docker push <your-dockerhub-repository>/subscribers
```

### Deploy required services

``` bash
./k8s/deployall.sh
```
This will deploy *postgres*, *prometheus*, *grafana* and all the other services declared under *./k8s*

### Verify that the deployment was successful

``` bash
kubectl get all
```
All services should show status *Running*.

```
kubectl logs pod/<pod-name>		# replace <pod-name> with the real name
```
This command will display logs of a given pod, this is useful in case there is an error

### Create the database

``` bash
kubectl exec -it deployment.apps/postgres -- bash
psql -U admin
create database subscribers;
exit
exit
```

## Tests

*tescontainers* module is used to bootstap a Postgresql database for tests.

## Bruno

Bruno is used as a replacement for Postman to test the api, all related files are located under *bruno* directory.
