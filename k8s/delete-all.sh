#!/bin/sh
kubectl delete -f "$(dirname "$0")"/otel-collector/
kubectl delete -f "$(dirname "$0")"/prometheus/
kubectl delete -f "$(dirname "$0")"/loki/
kubectl delete -f "$(dirname "$0")"/zipkin/
kubectl delete -f "$(dirname "$0")"/grafana/
kubectl delete -f "$(dirname "$0")"/postgres/
kubectl delete -f "$(dirname "$0")"/subscribers/
