#!/bin/sh
kubectl apply -f "$(dirname "$0")"/otel-collector/
kubectl apply -f "$(dirname "$0")"/prometheus/
kubectl apply -f "$(dirname "$0")"/loki/
kubectl apply -f "$(dirname "$0")"/zipkin/
kubectl apply -f "$(dirname "$0")"/grafana/
kubectl apply -f "$(dirname "$0")"/postgres/
kubectl apply -f "$(dirname "$0")"/subscribers/
