package org.djserr.subscriberservice;

import io.opentelemetry.instrumentation.spring.autoconfigure.instrumentation.annotations.InstrumentationAnnotationsAutoConfiguration;
import org.djserr.subscriberservice.subscriber.Subscriber;
import org.djserr.subscriberservice.subscriber.SubscriberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import io.micrometer.observation.annotation.Observed;

@EnableJpaAuditing
@SpringBootApplication(exclude = {InstrumentationAnnotationsAutoConfiguration.class}/* BUGFIX: bean collision */)
public class SubscriberApplication {

	private static final Logger LOG = LoggerFactory.getLogger(SubscriberApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(SubscriberApplication.class, args);
	}
	
	@Bean
	@Observed(name = "command.line.runner",
            contextualName = "commandLineRunner",
            lowCardinalityKeyValues = {"userType", "userType2"})
	CommandLineRunner runner(SubscriberRepository repo) {
		return args -> {
			var saved = repo.save(new Subscriber(null, "John Doe", "jdoe@yahoo.com", null, null));
			repo.delete(saved);
			LOG.info("CommandLineRunner: inserted then deleted subscriber " + saved.getId());
		};
	}
}

