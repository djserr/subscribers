package org.djserr.subscriberservice.subscriber;

public class SubscriberAlreadyExistsException extends RuntimeException {

	public SubscriberAlreadyExistsException() {
	}

	public SubscriberAlreadyExistsException(String message) {
		super(message);
	}
}
