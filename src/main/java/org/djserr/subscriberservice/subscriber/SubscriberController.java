package org.djserr.subscriberservice.subscriber;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/subscribers")
public class SubscriberController {

	private static final Logger LOG = LoggerFactory.getLogger(SubscriberController.class);
	
	private final SubscriberService subscriberService;

	public SubscriberController(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}

	@PostMapping
	public ResponseEntity<?> add(@RequestBody Subscriber newSubscriber) {
		var added = subscriberService.add(newSubscriber);
		LOG.info("Added new subscriber: {}", added.getId());
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(added.getId())
				.toUri())
				.build();
	}

	@GetMapping
	public List<Subscriber> subscribers() {
		return subscriberService.findAll();
	}

	@GetMapping("/{id}")
	public Subscriber subscriberById(@PathVariable UUID id) {
		return subscriberService.findById(id);
	}

	@PutMapping("/{id}")
	public Subscriber update(@PathVariable UUID id, @RequestBody @Valid Subscriber theUpdate) {
		var updated = subscriberService.update(id, theUpdate);
		LOG.info("Updated subscriber: " + updated.getId());
		return updated;
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable UUID id) {
		subscriberService.deleteById(id);
		LOG.info("Deleted subscriber: " + id);
	}
}
