package org.djserr.subscriberservice.subscriber;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
public class SubscriberControllerExceptionHandler extends ResponseEntityExceptionHandler {

	static final String SUBSCRIBER_NOT_FOUND = "Subscriber not found";
	static final String SUBSCRIBER_ALREADY_EXISTS = "Subscriber already exists";
	
	@ResponseBody
	@ExceptionHandler(SubscriberNotFoundException.class)
	public ResponseEntity<?> handleSubscriberNotFoundException(HttpServletRequest request, Throwable ex) {
		var errMsg = SUBSCRIBER_NOT_FOUND + (ex.getMessage() != null ? ": " + ex.getMessage() : "");
		var pd = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, errMsg);
		return new ResponseEntity<>(pd, HttpStatusCode.valueOf(pd.getStatus()));
	}
	
	@ResponseBody
	@ExceptionHandler(SubscriberAlreadyExistsException.class)
	public ResponseEntity<?> handleSubscriberAlreadyExistsException(HttpServletRequest request, Throwable ex) {
		var errMsg = SUBSCRIBER_ALREADY_EXISTS + (ex.getMessage() != null ? ": " + ex.getMessage() : "");
		var pd = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, errMsg);
		return new ResponseEntity<>(pd, HttpStatusCode.valueOf(pd.getStatus()));
	}
}