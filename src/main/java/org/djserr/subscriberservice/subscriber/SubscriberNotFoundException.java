package org.djserr.subscriberservice.subscriber;

public class SubscriberNotFoundException extends RuntimeException {

	public SubscriberNotFoundException() {
	}

	public SubscriberNotFoundException(String message) {
		super(message);
	}

}
