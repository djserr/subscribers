package org.djserr.subscriberservice.subscriber;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class SubscriberService {
	
	private final SubscriberRepository repo;
	
	public SubscriberService(SubscriberRepository repo) {
		this.repo = repo;
	}

	public List<Subscriber> findAll() {
		return repo.findAll();
	}

	public Subscriber findById(UUID id) {
		return repo.findById(id).orElseThrow(SubscriberNotFoundException::new);
	}
	
	public Subscriber add(Subscriber newSubscriber) {
		if (repo.findByEmail(newSubscriber.getEmail()).isPresent()) {
			throw new SubscriberAlreadyExistsException("this email is already used");
		}
		return repo.save(newSubscriber);
	}
	
	public Subscriber update(UUID id, Subscriber theUpdate) {
		return repo.findById(id)
			.filter(s -> s.getEmail().equals(theUpdate.getEmail()))
			.map(s -> {
				theUpdate.setId(id);
				return repo.save(theUpdate);
			})
			.orElseThrow(() -> new SubscriberNotFoundException("check id and email"));
	}
	
	public void deleteById(UUID id) {
		repo.deleteById(id);
	}
	
}
