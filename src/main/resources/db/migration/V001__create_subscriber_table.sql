CREATE TABLE IF NOT EXISTS "public"."subscriber" (
    "id" uuid NOT NULL,
    "email" character varying(255),
    "name" character varying(255),
    "creation_date" timestamp(6),
    "last_updated" timestamp(6),
    CONSTRAINT "subscriber_email_key" UNIQUE ("email"),
    CONSTRAINT "subscriber_pkey" PRIMARY KEY ("id")
)