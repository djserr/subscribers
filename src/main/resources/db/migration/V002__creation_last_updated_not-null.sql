ALTER TABLE "public"."subscriber"
ALTER COLUMN "creation_date" SET NOT NULL;

ALTER TABLE "public"."subscriber"
ALTER COLUMN "last_updated" SET NOT NULL;