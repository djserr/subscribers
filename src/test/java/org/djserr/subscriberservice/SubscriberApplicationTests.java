package org.djserr.subscriberservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(TestcontainerConfig.class)
class SubscriberApplicationTests {

	public static void main(String...args) {
		SpringApplication.from(SubscriberApplication::main).with(TestcontainerConfig.class).run(args);
	}

	@Test
	void contextLoads() {
	}

}
