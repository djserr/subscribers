package org.djserr.subscriberservice.subscriber;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.UUID;

import org.djserr.subscriberservice.TestcontainerConfig;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

@Import(TestcontainerConfig.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SubscriberControllerTest {

	@LocalServerPort int port;
	
	@Autowired JdbcTemplate jdbcTemplate;
	@Autowired SubscriberRepository repo;
	
	@Autowired RestClient.Builder restClientBuilder;
	RestClient restClient;
	
	@BeforeEach
	void setUp() {
		jdbcTemplate.execute("truncate table subscriber");
		var baseUrl = "http://localhost:" + port + "/api/subscribers";
		restClient = restClientBuilder.baseUrl(baseUrl).build();
	}
	
	@AfterEach
	void tearDown() {
	}

	@Test
	void shouldReturnEmptyListOfSubscribersWhenDBIsEmpty() {
		
		// When
		List<Subscriber> resp = restClient
				.get()
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.body(new ParameterizedTypeReference<>() {});
		
		// Then
		assertThat(resp).isEmpty();
	}

	@Test
	void shouldReturnAllSubscribers() {

		// Given
		var newSubscriber1 = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		var newSubscriber2 = new Subscriber(null, "Jane Doe", "jane.doe@gmail.com", null, null);
		var newSubscriber3 = new Subscriber(null, "Jonathan Doe", "jonathan.doe@outlook.com", null, null);
		
		var idx1 = repo.save(newSubscriber1).getId();
		var idx2 = repo.save(newSubscriber2).getId();
		var idx3 = repo.save(newSubscriber3).getId();

		// When
		var subscribers = restClient
				.get()
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.body(new ParameterizedTypeReference<List<Subscriber>>() {});
		
		// Then
		assertThat(subscribers).hasSize(3);
		var indices = subscribers.stream().map(Subscriber::getId).toList();
		assertThat(indices).contains(idx1, idx2, idx3);
	}
	
	@Test
	void shouldReturnSubscriberById() {
		
		// Given
		var subscriber = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		var idx = repo.save(subscriber).getId();
		
		// When
		var insertedSubscriber = restClient
				.get()
				.uri("/{id}", idx)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.body(Subscriber.class);
		
		// Then
		assertThat(insertedSubscriber).isNotNull();
		assertThat(insertedSubscriber.getId()).isEqualTo(idx);
		assertThat(insertedSubscriber.getName()).isEqualTo(subscriber.getName());
		assertThat(insertedSubscriber.getEmail()).isEqualTo(subscriber.getEmail());
	}

	@Test
	void shouldFailWhenAddingNewSubscriberWithAlreadyExistingEmail() {

		// Given 
		var newSubscriber1 = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		var newSubscriber2 = new Subscriber(null, "Jane Doe", "jdoe@hotmail.com", null, null);
		repo.save(newSubscriber1);
		
		// When
		ProblemDetail problemDetail = null;
		try {
			restClient.post()
					.contentType(MediaType.APPLICATION_JSON)
					.body(newSubscriber2)
					.accept(MediaType.APPLICATION_JSON)
					.retrieve()
					.toEntity(ProblemDetail.class);
		} catch(HttpClientErrorException ex) {
			problemDetail = ex.getResponseBodyAs(ProblemDetail.class);
		}
		
		// Then
		assertThat(problemDetail).as("Response from the POST should not be null.").isNotNull();
		assertThat(problemDetail.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		assertThat(problemDetail.getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
		assertThat(problemDetail.getDetail()).startsWith(SubscriberControllerExceptionHandler.SUBSCRIBER_ALREADY_EXISTS);
	}
	
	@Test
	void shouldDeleteExistingSubscriberById() {

		// Given
		var subscriberToDelete = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		var idx = repo.save(subscriberToDelete).getId();
		
		// When
		var resp = restClient.delete().uri("/{id}", idx).retrieve().toBodilessEntity();
		
		// Then
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(repo.existsById(idx)).isFalse();
	}
	
	@Test
	void shouldDeleteExistingSubscriberByIdIdempotently() {

		// Given
		var subscriberToDelete = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		var idx = repo.save(subscriberToDelete).getId();
		
		// When
		var resp1 = restClient.delete().uri("/{id}", idx).retrieve().toBodilessEntity();
		var resp2 = restClient.delete().uri("/{id}", idx).retrieve().toBodilessEntity();
		var resp3 = restClient.delete().uri("/{id}", idx).retrieve().toBodilessEntity();
		
		// Then
		assertThat(resp1.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(resp2.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(resp3.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(repo.existsById(idx)).isFalse();
	}
	
	@Test
	void shouldUpdateExistingSubscriber() {

		// Given
		var subscriber = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		subscriber = repo.save(subscriber);
		subscriber.setName("John Doe Jr");
		
		// When
		var resp = restClient.put()
				.uri("/{id}", subscriber.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.body(subscriber)
				.retrieve()
				.toEntity(Subscriber.class);
		
		// Then
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		var updatedSubscriber = resp.getBody();
		assertThat(updatedSubscriber.getName()).isEqualTo("John Doe Jr");
	}
	
	@Test
	void shouldUpdateExistingSubscriberIdempotently() {

		// Given
		var subscriber = new Subscriber(null, "John Doe", "jdoe@hotmail.com", null, null);
		subscriber = repo.save(subscriber);
		subscriber.setName("John Doe Jr");
		
		// When
		var resp1 = restClient.put()
				.uri("/{id}", subscriber.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.body(subscriber)
				.retrieve()
				.toEntity(Subscriber.class);
		var resp2 = restClient.put()
				.uri("/{id}", subscriber.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.body(subscriber)
				.retrieve()
				.toEntity(Subscriber.class);
		var resp3 = restClient.put()
				.uri("/{id}", subscriber.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.body(subscriber)
				.retrieve()
				.toEntity(Subscriber.class);
		
		// Then
		assertThat(resp1.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(resp2.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(resp3.getStatusCode()).isEqualTo(HttpStatus.OK);
		var updatedSubscriber1 = resp1.getBody();
		var updatedSubscriber2 = resp2.getBody();
		var updatedSubscriber3 = resp3.getBody();
		assertThat(updatedSubscriber1.getName()).isEqualTo("John Doe Jr");
		assertThat(updatedSubscriber2.getName()).isEqualTo("John Doe Jr");
		assertThat(updatedSubscriber3.getName()).isEqualTo("John Doe Jr");
	}

	@Test
	void shouldFailWhenUpdatingNonExistingSubscriber() {
		
		// Given
		var nonExistingsubscriber = new Subscriber(UUID.randomUUID(), "John Doe", "jdoe@hotmail.com", null, null);
		repo.delete(nonExistingsubscriber);
		
		// When
		Executable updateSubscriber = () -> restClient.put()
				.uri("/{id}", nonExistingsubscriber.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.body(nonExistingsubscriber)
				.retrieve()
				.toEntity(Subscriber.class);

		// Then
		var ex = assertThrows(HttpClientErrorException.class, updateSubscriber);
		var problemDetail = ex.getResponseBodyAs(ProblemDetail.class);
		assertThat(problemDetail.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
		assertThat(problemDetail.getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
		assertThat(problemDetail.getDetail()).startsWith(SubscriberControllerExceptionHandler.SUBSCRIBER_NOT_FOUND);
	}
}
